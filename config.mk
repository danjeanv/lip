
# http://www.howtoforge.com/ubuntu_pxe_install_server_p2

MENU_TITLE=LIP Polytech

BUILDDIR ?= $(CURDIR)/../build

DEB_MIRROR=http://ftp.fr.debian.org/debian
UBUNTU_MIRROR=http://archive.ubuntu.com/ubuntu
FEDORA_MIRROR=ftp://ftp.lip6.fr/pub/linux/distributions/fedora

NFS_ROOT=/srv/lip/nfs
NFS_SERVER=10.42.2.1:/srv/lip/nfs

PRESEED_URL=http://preseed.lip.private

WGET=wget --timestamping --directory-prefix=$(BUILDDIR)/downloaded

default_ARCHS=amd64 i386

KERN_OPTS= \
	debian-installer/language=fr \
	debian-installer/country=FR \
	mirror/country=FR \
	netcfg/get_domain=unassigned-domain \
	console-setup/layoutcode=fr \
	console-setup/variantcode=oss

$(eval $(call add-arch,amd64,Architechture 64bits (x86_64 / amd64)))
$(eval $(call add-arch,i386,Architechture 32bits (x86_32 / i386)))

$(eval $(call add-distrib,mint,Mint))
$(eval $(call add-distrib,ubuntu-live,Ubuntu Live))
$(eval $(call add-distrib,debian,Debian))
$(eval $(call add-distrib,ubuntu,Ubuntu Installer))
#$(eval $(call add-distrib,fedora,Fedora))

### Debian distrib
#$(eval $(call add-codename,bulleyse,debian,Bulleyse [Debian 11 testing],testing))
$(eval $(call add-codename,buster,debian,Buster [Debian 10 stable]))
#$(eval $(call add-codename,stretch,debian,Stretch [Debian 9 stable]))
#$(eval $(call add-codename,jessie,debian,Jessie [Debian 8 stable]))
#$(eval $(call add-codename,wheezy,debian,Whezzy [Debian 7 oldstable]))
#$(eval $(call add-codename,squeeze,debian,Squeeze [Debian 6 oldstable]))
### Ubuntu distrib
#$(eval $(call add-codename,disco-desktop,ubuntu-live,Disco Dingo [19.04 Desktop],,amd64))
$(eval $(call add-codename,focal-desktop,ubuntu-live,Focal Fossa [20.04.1 LTS Desktop],,amd64))
$(eval $(call add-codename,focal,ubuntu,Focal Fossa [20.04.1 LTS],,amd64))
#$(eval $(call add-codename,disco,ubuntu,Disco Dingo [19.04],,amd64))
#$(eval $(call add-codename,bionic-desktop,ubuntu-live,Bionic Beamer [18.04.1 LTS Desktop],,amd64))
#$(eval $(call add-codename,bionic,ubuntu,Bionic Beamer [18.04.3 LTS],,amd64))
#$(eval $(call add-codename,zesty-desktop,ubuntu-live,Zesty Zapus [17.04 Desktop]))
#$(eval $(call add-codename,zesty,ubuntu,Zesty Zapus [17.04]))
#$(eval $(call add-codename,xenial-desktop,ubuntu-live,Xenial Xerus [16.04.5 LTS Desktop],,i386))
#$(eval $(call add-codename,xenial,ubuntu,Xenial Xerus [16.04.5 LTS],,i386))
#$(eval $(call add-codename,vivid-desktop,ubuntu-live,Vivid Vervet [15.04 Desktop]))
#$(eval $(call add-codename,vivid,ubuntu,Vivid Vervet [15.04]))
#$(eval $(call add-codename,trusty-desktop,ubuntu-live,Trusty Tahr [14.04 LTS Desktop]))
#$(eval $(call add-codename,trusty,ubuntu,Trusty Tahr [14.04 LTS]))
#$(eval $(call add-codename,saucy,ubuntu,Saucy Salamander [13.10]))
#$(eval $(call add-codename,raring-desktop,ubuntu-live,Raring Ringtail [13.04 Desktop]))
#$(eval $(call add-codename,raring,ubuntu,Raring Ringtail [13.04]))
#$(eval $(call add-codename,quantal,ubuntu,Quantal Quetzal [12.10]))
#$(eval $(call add-codename,precise-desktop,ubuntu-live,Precise Pangolin [12.04 LTS Desktop]))
#$(eval $(call add-codename,precise,ubuntu,Precise Pangolin [12.04 LTS]))
#$(eval $(call add-codename,oneiric,ubuntu,Oneiric Ocelot))
#$(eval $(call add-codename,natty,ubuntu,Natty Narwhal))
#$(eval $(call add-codename,lucid,ubuntu,Lucid Lynx [10.04 LTS]))
### Fedora distrib
#$(eval $(call add-codename,14,fedora,Laughlin (Core 14)))
#$(eval $(call add-codename,15,fedora,Lovelock (Core 15)))
### Mint distrib
#$(eval $(call add-codename,qiana-cinnamon,mint,Qiana Cinnamon [Mint 17]))
#$(eval $(call add-codename,qiana-mate,mint,Qiana Mate [Mint 17]))
#$(eval $(call add-codename,rafaela-cinnamon,mint,Rafaela Cinnamon [Mint 17.2]))
#$(eval $(call add-codename,rafaela-mate,mint,Rafaela Mate [Mint 17.2]))

CLASSICS=focal buster
#CLASSICS=xenial-desktop jessie
#CLASSICS=vivid-desktop trusty-desktop jessie
#CLASSICS=qiana-cinnamon trusty-desktop wheezy
#CLASSICS=precise-desktop raring-desktop wheezy
#CLASSICS=raring-desktop precise-desktop wheezy
#CLASSICS=wheezy raring precise

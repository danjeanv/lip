
# http://www.howtoforge.com/ubuntu_pxe_install_server_p2

#######################################
# Get info from codename

# archs(codename)
archs=$($1-archs)

# url(codename,arch)
url=$(call $1-$2-src,$1,$2)

# dst(codename,arch)
dst=$($1-$2-dst)

# name(codename|distrib|arch,arch)
name=$($1-name)$(if $2, ($2))

# entry(codename,arch,menufile)
entry=$(call $(call distrib,$1)-entry,$1,$2,$3)

# distrib(codename)
distrib=$($1-distrib)

#######################################

# Setup new arch
define add-arch
#$$(warning Adding arch $1 / $2)
$1-name=$2
ARCHS+=$1
endef

# Setup new distrib
define add-distrib
#$$(warning Adding distrib $1 / $2)
$1-name=$2
DISTRIBS+=$1
fix-$1::

endef

# $(call debian-src,codename,arch)
debian-src=$(DEB_MIRROR)/dists/$1/main/installer-$2/current/images/netboot/debian-installer/$2
debian-testing-src=http://d-i.debian.org/daily-images/$2/daily/netboot/debian-installer/$2
#debian-testing-src=http://d-i.debian.org/daily-images/$2/$(if $(filter-out i386,$2),daily,20160904-00:46)/netboot/debian-installer/$2
debian-dst=debian/$1/$2
debian-entry=$(call entry-link-pxe,$1,$2,$3)

oldubuntu-src=$(UBUNTU_MIRROR)/dists/$1/main/installer-$2/current/images/netboot/ubuntu-installer/$2
oldubuntu-dst=ubuntu/$1/$2
oldubuntu-entry=$(call entry-link-pxe,$1,$2,$3)

ubuntu-src=$(UBUNTU_MIRROR)/dists/$1/main/installer-$2/current/legacy-images/netboot/ubuntu-installer/$2
ubuntu-dst=ubuntu/$1/$2
ubuntu-entry=$(call entry-link-pxe,$1,$2,$3)

# Workaround bug in NFS install for Bionic
# https://bugs.launchpad.net/ubuntu/+source/casper/+bug/1754777
# https://bugs.launchpad.net/ubuntu/+source/casper/+bug/1755863
ubuntu-live-src=
ubuntu-live-dst=ubuntu-live/$(patsubst %-live,%,$1)/$2
ubuntu-live-entry=\
    echo >> $3 "     KERNEL $(call dst,$1,$2)/vmlinuz" ;\
    echo >> $3 "     APPEND boot=casper rootfstype=nfs netboot=nfs nfsroot=$(NFS_SERVER)/$(call dst,$1,$2) $(KERN_OPTS) $(if $(filter bionic-desktop,$1),systemd.mask=tmp.mount) initrd=$(call dst,$1,$2)/initrd.lz -- splash quiet"

mint-src=
mint-dst=mint/$1/$2
mint-entry=\
    echo >> $3 "     KERNEL $(call dst,$1,$2)/vmlinuz" ;\
    echo >> $3 "     APPEND boot=casper rootfstype=nfs netboot=nfs nfsroot=$(NFS_SERVER)/$(call dst,$1,$2) $(KERN_OPTS) initrd=$(call dst,$1,$2)/initrd.lz -- splash quiet"

fedora-src=$(FEDORA_MIRROR)/releases/$1/Fedora/$(subst amd64,x86_64,$2)/os/images/pxeboot
fedora-dst=fedora/$1/$2

define add-codename # 1:codename 2:distrib 3:Name 4:variant 5:[archs]
#$$(warning Adding codename $1 / $2 / $3)
$1-distrib=$2
$1-name=$$(call fname,$2) $3
$1-archs=$(if $5,$5,$(if $($(call distrib,$1)-archs),$($(call distrib,$1)-archs),$(default_ARCHS)))
$$(foreach arch,$$($1-archs),$$(eval $$(call add-codename-arch,$1,$$(arch),$4)))
CODENAMES+=$1
endef

define add-codename-arch # 1:codename 2:arch 3:variant
#$$(warning Adding codename ARCH $1 / $2 / $3)
$1-$2-src=$$(call $$(call distrib,$1)$$(addprefix -,$3)-src,$1,$2)
$1-$2-dst=$$(call $$(call distrib,$1)-dst,$1,$2)
$$(eval $$(call download-dist,$1,$2))
DISTS_$2:=$$(filter-out $$(call distrib,$1),$(DISTS_$2)) $$(call distrib,$1)
CODENAMES_$$(call distrib,$1)_$2 += $1
fix-$1-$2:: fix-$1 fix-$2 fix-$$(call distrib,$1) fix-$$(call distrib,$1)-$2
fix-$1 fix-$2 fix-$$(call distrib,$1) fix-$$(call distrib,$1)-$2::
endef

# entry-link-pxe (1:codename 2:arch 3:menufile)
entry-link-pxe= \
	echo >> $3 "      KERNEL vesamenu.c32" ;\
	echo >> $3 "      APPEND $(call dst,$1,$2)/pxelinux.cfg/default"

COMMA:=,

define download-dist # dist arch
download-$1-$2:
	$$(if $$(call url,$1,$2),\
	$$(WGET) --no-host-directories \
		--cut-dirs=$$(shell expr $$$$(echo "$$(call url,$1,$2)" | tr -c -d '/' | wc -c ) - 2) \
		--recursive \
		--reject "pxelinux*$$(COMMA)index.html*" \
		--no-parent \
		--directory-prefix=$(BUILDDIR)/downloaded/$$(call dst,$1,$2) \
		$$(call url,$1,$2) \
	)
download-$1: download-$1-$2
download: download-$1
endef

NULL=
SPACE=$(NULL) $(NULL)

all: install

include config.mk

download-grubnetx64.efi:
	$(WGET) \
		--directory-prefix=$(BUILDDIR)/downloaded/ \
		http://archive.ubuntu.com/ubuntu/dists/bionic/main/uefi/grub2-amd64/current/grubnetx64.efi.signed
download: download-grubnetx64.efi

install: menus splash.png
	mkdir -p ../root ../www
	mkdir -p overwrite
	rsync -aHv --del $(BUILDDIR)/downloaded/. ../root/.
	rsync -aHv $(BUILDDIR)/downloaded-fix/. ../root/.
	rsync -aHv overwrite/. ../root/.
	rsync -aHv $(BUILDDIR)/preseed.cfg/. ../www/.
	rsync -aHv /usr/lib/PXELINUX/pxelinux.0 ../root/
	rsync -aHv /usr/lib/PXELINUX/lpxelinux.0 ../root/
	rsync -aHv pxechain.com ../root/
	rsync -aHv /usr/lib/syslinux/modules/bios/. ../root/
	rsync -aHv splash.png ../root/
	ln -fs grubnetx64.efi.signed ../root/bootx64.efi

downloaded::
	$(MAKE) download

fix:
	$(foreach arch,$(ARCHS),\
		$(foreach dist,$(filter $(DISTS_$(arch)),$(DISTRIBS)),\
			$(foreach cn,$(filter $(CODENAMES_$(dist)_$(arch)),$(CODENAMES)),\
				$(MAKE) --no-print-directory A=$(arch) D=$(dist) C=$(cn) fix-$(cn)-$(arch) && \
	))) true


menus: fix
	$(MAKE) $(BUILDDIR)/downloaded-fix/pxelinux.cfg/default 
	$(MAKE) $(BUILDDIR)/downloaded-fix/grub/grub.cfg

$(BUILDDIR)/downloaded-fix/grub/grub.cfg: $(BUILDDIR)/downloaded-fix/pxelinux.cfg/default syslinux2grub
	mkdir -p "$(dir $@)"
	cd $(BUILDDIR)/downloaded-fix && $(CURDIR)/syslinux2grub pxelinux.cfg/default > "$@"

$(BUILDDIR)/downloaded-fix/pxelinux.cfg/default: $(MAKEFILE_LIST) stdmenu.cfg
	mkdir -p $(dir $@)
	echo > $@ "UI vesamenu.c32"
	cat stdmenu.cfg >> $@
	echo >> $@ "MENU TITLE $(MENU_TITLE)"
	echo >> $@ "    LABEL line1" ;\
	echo >> $@ "      MENU LABEL Choix classiques 64bits" ;\
	echo >> $@ "      MENU DISABLE" ;\
	$(foreach arch,amd64,\
		$(foreach classic,$(CLASSICS),\
			$(foreach dist,$(filter $(DISTS_$(arch)),$(DISTRIBS)),\
				$(foreach cn,$(filter $(CODENAMES_$(dist)_$(arch)),$(classic)),\
					echo >> $@ "    LABEL main/$(dist)" ;\
					echo >> $@ "      MENU LABEL $(call name,$(dist)) :$(call name,$(cn))" ;\
					echo >> $@ "      MENU INDENT 1" ;\
					$(call entry,$(cn),$(arch),$@) ;\
	)))) \
	echo >> $@ "    LABEL line2" ;\
	echo >> $@ "      MENU LABEL Chainage" ;\
	echo >> $@ "      MENU DISABLE" ;\
	echo >> $@ "      LABEL disk" ;\
	echo >> $@ "        MENU INDENT 1" ;\
	echo >> $@ "        MENU LABEL Disque dur local" ;\
	echo >> $@ "        LOCALBOOT 0" ;\
	echo >> $@ "      LABEL biosnext" ;\
	echo >> $@ "        MENU INDENT 1" ;\
	echo >> $@ "        MENU LABEL Choix suivant du BIOS" ;\
	echo >> $@ "        LOCALBOOT -1" ;\
	echo >> $@ "    LABEL line3" ;\
	echo >> $@ "      MENU LABEL Choix experts" ;\
	echo >> $@ "      MENU DISABLE" ;\
	$(foreach arch,$(ARCHS),\
		echo >> $@ "MENU BEGIN" ;\
		echo >> $@ "  MENU TITLE $(call name,$(arch))" ;\
		$(foreach dist,$(filter $(DISTS_$(arch)),$(DISTRIBS)),\
			echo >> $@ "    LABEL $(arch)" ;\
			echo >> $@ "      MENU LABEL $(call name,$(dist))" ;\
			echo >> $@ "      MENU DISABLE" ;\
			$(foreach cn,$(filter $(CODENAMES_$(dist)_$(arch)),$(CODENAMES)),\
				echo >> $@ "    LABEL $(dist)/$(cn)/$(arch)" ;\
				echo >> $@ "      MENU LABEL $(call name,$(dist))$(call name,$(cn))" ;\
				echo >> $@ "      MENU INDENT 1" ;\
				$(call entry,$(cn),$(arch),$@) ;\
		)) \
		echo >> $@ "MENU END" ;\
	)

clean::
	$(RM) -rfv $(BUILDDIR)/downloaded $(BUILDDIR)/downloaded-fix $(BUILDDIR)/preseed.cfg

####################################################################################
ifneq ($(A),)
ifneq ($(C),)
ifneq ($(D),)

fix-debian fix-ubuntu:: fix-%:
	$(MAKE) fix-installer-path FIP=1 $(BUILDDIR)/preseed.cfg/$D/$C/$A/preseed.cfg

fix-ubuntu-live:: fix-%:
	$(MAKE) copy-livecd-kernel CPK=1

fix-mint:: fix-%:
	$(MAKE) copy-livecd-kernel CPK=1

ifneq ($(FIP),)
fix-installer-path: $(shell find $(BUILDDIR)/downloaded/$D/$C/$A -type f -a \( -iname "*.cfg" -o -iname default \) -printf "$(BUILDDIR)/downloaded-fix/$D/$C/$A/%P\n")
endif

$(BUILDDIR)/downloaded-fix/debian/%: $(BUILDDIR)/downloaded/debian/% $(MAKEFILE_LIST)
	@mkdir -p $(dir $@)
	sed < $< \
	 > $@ \
	 -e 's,debian-installer/[^/]*/,$(subst $(SPACE),/,$(wordlist 2, 4, $(subst /,$(SPACE),$(patsubst $(BUILDDIR)/%,%,$<))))/,' \
	 -e 's,append vga=,append $(KERN_OPTS) preseed/url=$(PRESEED_URL)/$D/$C/$A/preseed.cfg vga=,'

$(BUILDDIR)/downloaded-fix/ubuntu/%: $(BUILDDIR)/downloaded/ubuntu/% $(MAKEFILE_LIST)
	@mkdir -p $(dir $@)
	sed < $< \
	 > $@ \
	 -e 's,ubuntu-installer/[^/]*/,$(subst $(SPACE),/,$(wordlist 2, 4, $(subst /,$(SPACE),$(patsubst $(BUILDDIR)/%,%,$<))))/,' \
	 -e 's,append vga=,append $(KERN_OPTS) preseed/url=$(PRESEED_URL)/$D/$C/$A/preseed.cfg vga=,'

$(BUILDDIR)/downloaded-fix/ubuntu-live/%: $(BUILDDIR)/downloaded/ubuntu-live/% $(MAKEFILE_LIST)
	@mkdir -p $(dir $@)
	sed < $< \
	 > $@ \
	 -e 's,ubuntu-installer/[^/]*/,$(subst $(SPACE),/,$(wordlist 2, 4, $(subst /,$(SPACE),$(patsubst $(BUILDDIR)/%,%,$<))))/,' \
	 -e 's,append vga=,append $(KERN_OPTS) preseed/url=$(PRESEED_URL)/$D/$C/$A/preseed.cfg vga=,'

$(BUILDDIR)/preseed.cfg/$D/$C/$A/preseed.cfg: $(MAKEFILE_LIST) $(wildcard $(addprefix preseed.cfg.d/,base.cfg $D.cfg $C.cfg $A.cfg $D-$A.cfg $C-$A.cfg))
	@mkdir -p "$(dir $@)"
	cat $(filter %.cfg,$^) > "$@"

ifneq ($(CPK),)
copy-livecd-kernel: $(BUILDDIR)/downloaded-fix/$D/$C/$A/initrd.lz $(BUILDDIR)/downloaded-fix/$D/$C/$A/vmlinuz

$(BUILDDIR)/downloaded-fix/ubuntu-live/bionic-desktop/amd64/initrd.lz: ubuntu-live-bionic-initrd.lz
	@mkdir -p "$(dir $@)"
	cp -f "$<" "$@"

%$(BUILDDIR)/downloaded-fix/$D/$C/$A/initrd.lz: $(NFS_ROOT)/$D/$C/$A/casper/initrd.lz
	@mkdir -p "$(dir $@)"
	cp -f "$<" "$@"

%$(BUILDDIR)/downloaded-fix/$D/$C/$A/initrd.lz: $(NFS_ROOT)/$D/$C/$A/casper/initrd
	@mkdir -p "$(dir $@)"
	cp -f "$<" "$(@:.lz=)"
	lz "$(@:.lz=)"

$(BUILDDIR)/downloaded-fix/$D/$C/$A/vmlinuz: $(wildcard $(NFS_ROOT)/$D/$C/$A/casper/vmlinuz*)
	@mkdir -p "$(dir $@)"
	cp -f "$<" "$@"

$(NFS_ROOT)/$D/$C/$A/casper/initrd.lz:
	$(warning You must download the Ubuntu Live CD image and mount it with loopback)
	$(warning Looking for $@)
	$(error Aborting.)

endif

M=$(BUILDDIR)/downloaded-fix/$D/$C/$A/pxelinux.cfg/default
fix-fedora::
	mkdir -p $(dir $(M))
	echo > $M "UI vesamenu.c32"
	echo >> $M "MENU TITLE $(call name,$(call distrib,$(CODENAME))) [Install Party]"
	echo >> $M "    LABEL default"
	echo >> $M "      MENU LABEL Default"
	echo >> $M "      kernel $(call distrib,$(CODENAME))/$(CODENAME)/$(ARCH)/vmlinuz"
	echo >> $M "      append initrd=$(call distrib,$(CODENAME))/$(CODENAME)/$(ARCH)/initrd.img"
	for e in $(wildcard downloaded/$(call distrib,$(CODENAME))/$(CODENAME)/$(ARCH)/vmlinuz-*) ; do \
		l=$$(basename "$$e" | sed -e 's/^vmlinuz-//') ;\
		L="$$l" ;\
		echo >> $M "    LABEL $$l" ;\
		echo >> $M "      MENU LABEL $$L" ;\
		echo >> $M "      kernel $(call distrib,$(CODENAME))/$(CODENAME)/$(ARCH)/vmlinuz-$$l" ;\
		echo >> $M "      append initrd=$(call distrib,$(CODENAME))/$(CODENAME)/$(ARCH)/initrd-$$l.img" ;\
	done

endif
endif
endif

